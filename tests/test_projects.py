import requests
from helpers.utils import assertKeysIn
from helpers.config import settings



def test_get_projects():
    response = requests.get(f'{settings.api_url}/projects', headers={'api_key':settings.api_key})
    assert response.ok, response.text
    assert 4==len(response.json()['response']), 'Request returned more than 4 projects'

def test_get_project():
    response = requests.get(f'{settings.api_url}/projects/1143', headers={'api_key':settings.api_key})
    projects = response.json()['response']
    assert response.ok, response.text
    assert 1==len(projects)
    assertKeysIn(['id','reservation','name','name','objective', 'city','investors','energy_generated','province','description','amount_assigned'], projects[0])
