import requests
import pytest
from helpers.config import settings

def _assertKeysIn(keys = [], dict={}):
    for key in keys:
        assert key in dict

def test_get_ecooo_info():
    response = requests.get(f'{settings.api_url}/ecooo-info', headers={'api_key':settings.api_key})
    ecooo_info = response.json()
    assert response.ok, response.text
    _assertKeysIn(['tn_co2_avoided','installed_power','energy_generated', 'plant_participants', 'plants_with_reservation', 'total_installations', 'total_inversion', 'total_benefits', 'total_investors'], ecooo_info)
