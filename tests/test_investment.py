import requests
import json
import pytest
from helpers.utils import assertKeysIn
from helpers.config import settings
from helpers import odoo_xmlrpc


def __post_client_model(model_file_location):
    with open(model_file_location) as file:
        client = json.load(file)
        
    response = requests.post(f'{settings.api_url}/contract/create', json=client, headers={'api_key':settings.api_key})
    assert response.ok, response.text
    return response.json()['result']

@pytest.mark.parametrize(
    'investment_path', 
    [
        './tests/fixtures/investment/individual.json'
    ]
)
def test_individual_client(investment_path):
    result = __post_client_model(investment_path)
    assertKeysIn(['contract_id', 'partner_id', 'partner_vat'], result)

    # Delete created contract and partner
    odoo_xmlrpc.remove_contracts_and_partners(contract_ids=[result['contract_id']], partner_ids=[result['partner_id']])

@pytest.mark.parametrize(
    'investment_path', 
    [
        './tests/fixtures/investment/individual.json'
    ]
)
def test_individual_existing_client(investment_path):
    first_result = __post_client_model(investment_path)
    assertKeysIn(['contract_id', 'partner_id', 'partner_vat'], first_result)
    
    second_result = __post_client_model(investment_path)
    assertKeysIn(['contract_id', 'partner_id', 'partner_vat'], second_result)

    assert first_result['partner_id'] == second_result['partner_id']

    #Delete created contract and partner
    odoo_xmlrpc.remove_contracts_and_partners(contract_ids=[first_result['contract_id'], second_result['contract_id']], partner_ids=[first_result['partner_id']])


@pytest.mark.parametrize(
    'investment_path', 
    [
        './tests/fixtures/investment/minor.json',
        './tests/fixtures/investment/marriage.json',
        './tests/fixtures/investment/partnership.json'
    ]
)
def test_double_client(investment_path):
    result = __post_client_model(investment_path)
    assertKeysIn(['contract_id', 'partner_id', 'partner_vat', 'partner2_id', 'partner2_vat'], result)

    # Delete created contract and partners
    odoo_xmlrpc.remove_contracts_and_partners(contract_ids=[result['contract_id']], partner_ids=[result['partner_id'], result['partner2_id']])
