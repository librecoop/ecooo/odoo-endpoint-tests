import requests
import json
import pytest
from helpers.utils import assertKeysIn
from helpers.config import settings
from helpers import odoo_xmlrpc

@pytest.mark.parametrize(
    'contact_path',
    [
        './tests/fixtures/contact/contact.json',
        './tests/fixtures/contact/colective_autoconsumption_info.json',
        './tests/fixtures/contact/colective_autoconsumption_estimate.json',
        './tests/fixtures/contact/autoconsumption_info.json',
        './tests/fixtures/contact/autoconsumption_estimate.json'
    ]
)
def test_post_contact(contact_path):
    with open(contact_path) as file:
        contact = json.load(file)
    response = requests.post(f'{settings.api_url}/partner/create', json=contact, headers={'api_key':settings.api_key})
    assert response.ok, response.text
    result = response.json()['result']
    assertKeysIn(['partner_id', 'partner_email'], result)

    # Delete created parnter
    odoo_xmlrpc.remove_partner(result['partner_id'])
