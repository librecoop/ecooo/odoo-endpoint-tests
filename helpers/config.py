import json
from types import SimpleNamespace

with open ('config.json') as test_config_file:
    settings = json.load(test_config_file, object_hook=lambda d: SimpleNamespace(**d))
