import xmlrpc.client
from helpers.config import settings


def perform_rpc_action(function, **kwargs):
    with xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(settings.api_url)) as common:
        uid = common.authenticate(settings.odoo_db, settings.odoo_user, settings.odoo_pwd, {})
    with xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(settings.api_url)) as models:
        result = function(models, uid, **kwargs)
    return result


def remove_contracts_and_partners(contract_ids = [], partner_ids = []):
    perform_rpc_action(__remove_contracts_and_partners, contract_ids=contract_ids, partner_ids=partner_ids)


def remove_partner(partner_id):
    perform_rpc_action(
        lambda models, uid:
            models.execute_kw(settings.odoo_db, uid, settings.odoo_pwd, 'res.partner', 'unlink', [partner_id])
    )


def __remove_contracts_and_partners(models, uid, contract_ids = [], partner_ids = []):
    for contract_id in contract_ids:
        models.execute_kw(settings.odoo_db, uid, settings.odoo_pwd, 'contract.participation', 'unlink', [contract_id])
    
    for partner_id in partner_ids:
        models.execute_kw(settings.odoo_db, uid, settings.odoo_pwd, 'res.partner', 'unlink', [partner_id])
