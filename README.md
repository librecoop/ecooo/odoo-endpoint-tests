# Odoo Endpoint Test
Python project to automate the test of Odoo API endpoints

## Configuration file
In order to correctly run the tests the following parameters must be set in config.json

```json
{
  "api_url":[url_of_odoo_instance],
  "xmlrcp_port":[xmlrpc_port_for_deletion],
  "odoo_user":[odoo_username],
  "odoo_pwd":[odoo_password],
  "odoo_db":[odoo_database_name]
}
```

## Setup Instructions

Default setup and settings are prepared for local development - better in a Linux machine :)

0. (Optional) Set up a python3 virtual environment
```shell
virtualenv -p python3 .venv
```
Note: (Windows users might prefer to use **Conda** for this)

1. Activate python venv
```shell
source .venv/bin/activate
```

2. Gather python requirements
```shell
pip install -r requirements.txt
```
3. Run the tests
```shell
pytest
```
